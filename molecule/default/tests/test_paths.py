def test_project_path_created(host):
    project = host.file('/var/app/internewshid')
    assert project.exists
    assert project.is_directory
