def test_local_settings_linked(host):
    local_settings = host.file(
        '/var/app/internewshid/internewshid/local_settings.py'
    )
    assert local_settings.exists
    assert local_settings.is_symlink


def test_private_settings_created(host):
    private_settings = host.file(
        '/var/app/internewshid/internewshid/private_settings.py'
    )
    assert private_settings.exists


def test_private_settings_has_keys(host):
    private_settings = host.file(
        '/var/app/internewshid/internewshid/private_settings.py'
    )
    assert private_settings.contains('SECRET_KEY')
    assert private_settings.contains('DB_PASSWORD')
