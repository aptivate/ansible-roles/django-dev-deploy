[![pipeline status](https://git.coop/aptivate/ansible-roles/django-dev-deploy/badges/master/pipeline.svg)](https://git.coop/aptivate/ansible-roles/django-dev-deploy/commits/master)

# django-dev-deploy

A role for deploying Django on our local machines.

Only Debian Stretch and Ubuntun 18.04 are supported so far.

# Requirements

* System: `git`, `mysql-server` and `python-pip`.
* Python: [pipenv](https://pipenv.readthedocs.io/en/latest/install/)

# Role Variables

## Mandatory

  * `django_dev_project_root`: The absolute path of the Django project.

  * `django_dev_project_name`: The name of the Django project.

  * `mysql_root_password`: The MySQL root password.

# Dependencies

None.

# Example Playbook

```yaml
- hosts: localhost
  roles:
     - role: django-dev-deploy
       django_dev_project_name: aptivate
       django_dev_project_root: /var/django/aptivate
       mysql_root_password: foobar
```

# Testing

```bash
$ pipenv run molecule test
```

# License

  * https://www.gnu.org/licenses/gpl-3.0.en.html

# Author Information

  * https://aptivate.org/
  * https://git.coop/aptivate
